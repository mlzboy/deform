package com.jjxliu.deform.service;

import java.util.List;

import com.jjxliu.deform.pojo.UeEntry;
import com.jjxliu.deform.pojo.UeForm;

public interface UeService {

	/**
	 * 添加一个动态表单
	 * @param form
	 */
	public String addForm(UeForm form)  ;
	
	/**
	 * 查找具体的自定义表单
	 * @param form_id 表单ID
	 * @param entry_id 工单ID
	 * @return
	 */
	public UeForm findByFormId(int form_id );
	
	/**
	 * 删除一个动态表单
	 * @param form_id
	 */
	public String removeForm(int form_id);
	
	/**
	 * 更新一个动态表单
	 * @param form
	 */
	public String updateForm(UeForm form);
	
	
	/**
	 * 查询所有有创建的动态表单集合
	 * @return
	 */
	public List<UeForm> findAllForms();
	
	
	/**
	 * 增加一个工单种类
	 * @param entry
	 * @return
	 */
	public String addEntry(UeEntry entry);
	
	/**
	 * 删除一个工单
	 * @param id 
	 */
	public void removeEntry(int id);
	
	/**
	 * 更新工单 目前主要是名称
	 * @param entry
	 * @return
	 */
	public String updateEntry(UeEntry entry);
	
	/**
	 * 
	 * @return
	 */
	public List<UeEntry> findAllEntry() ;
	
	/**
	 * 根据ID 查找工参
	 * @param entry_id
	 * @return
	 */
	public UeEntry findById(int entry_id);
	
}
